import {Module} from '@nestjs/common';
import {CustomersService} from './customers.service';
import {CustomersController} from './customers.controller';
import {SequelizeModule} from "@nestjs/sequelize";
import {Customer} from "./customer.model";
import {CustomersFilms} from "./customersFilms.model";
import {UsersModule} from "../users/users.module";

@Module({
    imports: [SequelizeModule.forFeature([Customer, CustomersFilms]), UsersModule],
    providers: [CustomersService],
    controllers: [CustomersController],
    exports: [CustomersService]
})
export class CustomersModule {
}
