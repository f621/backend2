import {Injectable} from '@nestjs/common';
import {InjectModel} from "@nestjs/sequelize";
import {CustomersFilms} from "./customersFilms.model";
import {Customer} from "./customer.model";
import {User} from "../users/user.model";
import {UsersService} from "../users/users.service";

@Injectable()
export class CustomersService {

    constructor(@InjectModel(CustomersFilms) private customersFilms: typeof CustomersFilms,
                private readonly usersService: UsersService) {
    }

    async rent(days: number, calculationObject: any, filmId: number, customerId: number, points: number, usePoints: boolean) {
        const CF = await this.customersFilms.create({
            filmId,
            customerId,
            startDate: new Date(),
            netTotal: usePoints ? calculationObject.pointsPrice : calculationObject.normalPrice,
            numberOfDaysPayedWithPoints: usePoints ? calculationObject.discountsDays : 0,
            restored: false,
            numberOfDays: days,
        });

        // remove the used points
        // add point to the customer
        await this.usersService.addPoints(customerId, points - (usePoints ? calculationObject.discountsDays * 25 : 0));
        return CF;
    }

    async index() {
        return this.usersService.findAll();
    }

    async myRents(user: any) {
        return this.usersService.getUserWithRentedFilms(user.id);
    }

    async customerRents(id: number) {
        return Promise.resolve(undefined);
    }

    async getPoints(id) {
        return await this.usersService.getPointsByPk(id);
    }
}
