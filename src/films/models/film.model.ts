import {
    AutoIncrement,
    BelongsTo,
    Column,
    ForeignKey,
    HasMany,
    Model,
    PrimaryKey,
    Table,
    Unique
} from "sequelize-typescript";
import {FilmType} from "./filmType.model";
import {CustomersFilms} from "../../customers/customersFilms.model";

@Table
export class Film extends Model {
    @PrimaryKey
    @AutoIncrement
    @Column
    id: number;

    @Column
    name: string;

    @Column
    rate: number;

    @Column
    status: string;
    @ForeignKey(() => FilmType)
    @Column
    filmTypeId: number;

    @BelongsTo(() => FilmType)
    filmType: FilmType;

    @HasMany(() => CustomersFilms)
    customerFilms: CustomersFilms[];
}