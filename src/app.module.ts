import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {AuthModule} from './auth/auth.module';
import {UsersModule} from './users/users.module';
import {SequelizeModule} from '@nestjs/sequelize';
import {FilmsModule} from './films/films.module';
import {CustomersModule} from './customers/customers.module';

@Module({
    imports: [AuthModule, UsersModule,
        SequelizeModule.forRoot({
            dialect: "mysql",
            host: "localhost",
            port: 3306,
            database: 'filmcenter2',
            username: "sa",
            password: "sasa",
            models: [],
            autoLoadModels: true,
            synchronize: true,
            // sync: {alter: true}
        }),
        FilmsModule,
        CustomersModule
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {
}
