import {BelongsTo, Column, ForeignKey, HasMany, Model, PrimaryKey, Table} from "sequelize-typescript";
import {Film} from "./film.model";

@Table
export class FilmType extends Model {
    @PrimaryKey
    @Column
    id: number;
    @Column
    name: string;
    @Column
    price: number;
    @Column
    numberOfFirstDays: number;
    @Column
    points: number;
    @HasMany(() => Film)
    films: Film[];
}