import {Body, Controller, Delete, Get, Param, Post, Put, Request, UseGuards} from '@nestjs/common';
import {CreateFilmDto} from "./dto/create-film.dto";
import {FilmsService} from "./films.service";
import {CustomersService} from "../customers/customers.service";
import {JwtAuthGuard} from "../auth/guards/jwt-auth.guard";
import {Roles} from "../users/roles.decorator";
import {Role} from "../users/role.enum";
import {RolesGuard} from "../auth/guards/roles.guard";

@Controller('films')
export class FilmsController {


    constructor(private readonly filmsService: FilmsService) {
    }

    @Get('types')
    @UseGuards(RolesGuard)
    @UseGuards(JwtAuthGuard)
    @Roles(Role.Admin)
    async getFilmsTypes(){
        return await this.filmsService.getFilmsTypes();
    }

    @UseGuards(RolesGuard)
    @UseGuards(JwtAuthGuard)
    @Get()
    async getAllFilms() {
        return await this.filmsService.index();
    }
    @UseGuards(RolesGuard)
    @UseGuards(JwtAuthGuard)
    @Post()
    async createFilm(@Body() createFilmDto: CreateFilmDto) {
        createFilmDto.status = 'available';
        return await this.filmsService.store(createFilmDto);
    }
    @UseGuards(RolesGuard)
    @UseGuards(JwtAuthGuard)
    @Delete(':id')
    async delete(@Param('id') id: number) {
        return await this.filmsService.delete(id);
    }
    @UseGuards(RolesGuard)
    @UseGuards(JwtAuthGuard)
    @Put(':id')
    async update(@Param('id') id: number, @Body('filmTypeId') filmTypeId: number) {
        return await this.filmsService.update(id, filmTypeId);
    }
    @UseGuards(RolesGuard)
    @UseGuards(JwtAuthGuard)
    @Get('renting')
    async getAllRentingOperations() {
        return await this.filmsService.getAllRentingOperations();
    }
    /** Customer Area */

    @UseGuards(JwtAuthGuard)
    @Post('/:id/calculate')
    async calculate(@Param('id') id: number, @Body('days') days: number, @Request() req: any) {
        return await this.filmsService.calculate(id, days, req.user);
    }

    @UseGuards(JwtAuthGuard)
    @Post('/:id/rent')
    async rent(@Param('id') id: number, @Body('days') days: number,
               @Body('points') usePoints: boolean, @Request() req) {
        return await this.filmsService.rent(id, days, req.user, usePoints);
    }

    @Get('available')
    async available() {
        return await this.filmsService.available();
    }


}
