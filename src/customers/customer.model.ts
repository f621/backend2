import {Column, Model, PrimaryKey, Table} from "sequelize-typescript";

@Table
export class Customer extends Model {
    @PrimaryKey
    @Column
    id: number;

    @Column
    fullName: string;

    @Column
    phoneNumber: number;

    @Column
    address: number;

    @Column
    points: number;
}