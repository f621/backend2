import {HttpException, HttpStatus, Injectable, UseGuards} from '@nestjs/common';
import {CreateFilmDto} from "./dto/create-film.dto";
import {InjectModel} from "@nestjs/sequelize";
import {Film} from "./models/film.model";
import {FilmType} from "./models/filmType.model";
import {max} from "rxjs";
import {CustomersService} from "../customers/customers.service";
import {CustomersFilms} from "../customers/customersFilms.model";
import {User} from "../users/user.model";
import {RolesGuard} from "../auth/guards/roles.guard";

@Injectable()
export class FilmsService {
    constructor(
        @InjectModel(Film) private filmModel: typeof Film,
        @InjectModel(FilmType) private filmType: typeof FilmType,
        private readonly customerService: CustomersService
    ) {
    }

    async store(createFilmDto: CreateFilmDto) {
        return await this.filmModel.create(createFilmDto);
    }

    async index() {
        return await this.filmModel.findAll({include: [FilmType]});
    }

    async available() {
        return await this.filmModel.findAll({where: {status: "Available"}, include: [FilmType]});
    }

    async delete(id: number) {
        return await this.filmModel.destroy({where: {id: id}});
    }

    async update(id: number, filmTypeId: number) {
        return await this.filmModel.update({
            filmTypeId
        }, {where: {id: id}});
    }

    async calculate(id: number, days: number, user: any) {
        const film = await this.filmModel.findOne({where: {id}, include: [FilmType]});
        const normalPrice = film.filmType.price + (film.filmType.price *
            Math.max(days - film.filmType.numberOfFirstDays, 0));
        let pointsPrice = normalPrice;
        if (film.filmType.id == 1) {
            const customerPoints = await this.customerService.getPoints(user.id);
            if (customerPoints >= 25) {
                const discountsDays = Math.floor(customerPoints / 25);
                if (days - discountsDays > 0) {
                    days = days - discountsDays;
                    pointsPrice = film.filmType.price + (film.filmType.price *
                        Math.max(days - film.filmType.numberOfFirstDays, 0));
                } else
                    pointsPrice = 0;

                return {discountsDays, pointsPrice, normalPrice};
            }
        }

        return {discountsDays: null, pointsPrice: null, normalPrice};
    }

    async rent(filmId: number, days: number, user: any, usePoints: boolean) {
        const calculationObject = await this.calculate(filmId, days, user);
        const film = (await this.filmModel.findByPk(filmId, {include: [FilmType]}));
        if (film.status != 'Available') {
            throw new HttpException('Film is Out Of Stock', HttpStatus.BAD_REQUEST);
        }
        const addedPoints = film.filmType.points;
        const CF = await this.customerService.rent(days, calculationObject, filmId, user.id, addedPoints, usePoints);
        await this.filmModel.update({status: 'OutOfStock'}, {where: {id: filmId}})
        return CF;
    }

    async getAllRentingOperations() {
        return this.filmModel.findAll({
            include: [{
                model: CustomersFilms,
                include: [User]
            }]
        });
    }

    async getFilmsTypes() {
        return await this.filmType.findAll();
    }
}
