import {Controller, Get, Param, Request, UseGuards} from '@nestjs/common';
import {CustomersService} from "./customers.service";
import {JwtAuthGuard} from "../auth/guards/jwt-auth.guard";

@Controller('customers')
export class CustomersController {

    constructor(private readonly customersService: CustomersService) {
    }

    @UseGuards(JwtAuthGuard)
    @Get()
    async index() {
        return await this.customersService.index();
    }

    @UseGuards(JwtAuthGuard)
    @Get('rents')
    async myRents(@Request() req: any) {
        return await this.customersService.myRents(req.user);
    }

    @UseGuards(JwtAuthGuard)
    @Get('/:id/rents')
    async customerRents(@Param('id') id: number) {
        return await this.customersService.customerRents(id);
    }


}
