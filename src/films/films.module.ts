import {Module} from '@nestjs/common';
import {FilmsService} from './films.service';
import {FilmsController} from './films.controller';
import {SequelizeModule} from "@nestjs/sequelize";
import {Film} from "./models/film.model";
import {FilmType} from "./models/filmType.model";
import {CustomersModule} from "../customers/customers.module";

@Module({
    imports: [SequelizeModule.forFeature([Film, FilmType]),
    CustomersModule],
    providers: [FilmsService],
    controllers: [FilmsController]
})
export class FilmsModule {
}
