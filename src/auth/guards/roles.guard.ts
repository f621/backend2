import {Injectable, CanActivate, ExecutionContext, Request} from '@nestjs/common';
import {Reflector} from '@nestjs/core';
import {Role} from "../../users/role.enum";
import {ROLES_KEY} from "../../users/roles.decorator";
import {UsersService} from "../../users/users.service";

@Injectable()
export class RolesGuard implements CanActivate {
    constructor(private reflector: Reflector
    ) {
    }

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const requiredRoles = this.reflector.get<Role[]>(ROLES_KEY,
            context.getHandler());
        if (!requiredRoles) {
            return true;
        }
        const request = context.switchToHttp().getRequest();
        return request.user.username === 'admin';
        //return await this.userService.checkRoles(request.user.id);
    }
}