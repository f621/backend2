import {Injectable} from '@nestjs/common';
import {User} from "./user.model";
import {InjectModel} from "@nestjs/sequelize";
import {CustomersFilms} from "../customers/customersFilms.model";
import {Film} from "../films/models/film.model";
import {FilmType} from "../films/models/filmType.model";
import {Role} from "./role.enum";

@Injectable()
export class UsersService {

    constructor(
        @InjectModel(User)
        private userModel: typeof User) {
    }

    async findOne(username: string): Promise<User | undefined> {
        return this.userModel.findOne({where: {username}});
    }

    async getUserWithRentedFilms(id: number): Promise<User | undefined> {
        return this.userModel.findOne({
            where: {id}, include: [
                {
                    model: CustomersFilms,
                    include: [{model: Film, include: [FilmType]}]
                }
            ]
        });
    }

    async findAll(): Promise<User[] | undefined> {
        return this.userModel.findAll();
    }

    async addPoints(customerId: number, points: number) {
        const customer = await this.userModel.findByPk(customerId)
        customer.points = customer.points + points;
        return await customer.save();
    }

    async getPointsByPk(id: number) {
        return (await this.userModel.findByPk(id)).points;
    }

    async checkRoles(userId: number, ...roles: Role[]) {
        const user = await this.userModel.findByPk(userId);
        return user.role === roles[0];
    }
}
