import {Column, HasMany, Model, PrimaryKey, Table, Unique} from "sequelize-typescript";
import {CustomersFilms} from "../customers/customersFilms.model";

@Table
export class User extends Model {
    @PrimaryKey
    @Column
    id: number;

    @Unique
    @Column
    username: string;

    @Column
    password: string;

    @Column
    name: string;

    @Column
    role: string;

    @Column
    points: number;

    @HasMany(() => CustomersFilms)
    customerFilms: CustomersFilms[];
}