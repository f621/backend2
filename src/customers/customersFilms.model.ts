import {AutoIncrement, BelongsTo, Column, ForeignKey, HasMany, Model, PrimaryKey, Table} from "sequelize-typescript";
import {Film} from "../films/models/film.model";
import {Customer} from "./customer.model";
import {User} from "../users/user.model";

@Table
export class CustomersFilms extends Model {
    @PrimaryKey
    @AutoIncrement
    @Column
    id: number;

    @ForeignKey(() => User)
    @Column
    customerId: string;

    @ForeignKey(() => Film)
    @Column
    filmId: number;

    @Column
    startDate: Date;

    @Column
    numberOfDays: number;

    @Column
    restored: boolean;

    @Column
    numberOfDaysPayedWithPoints: number;

    @Column
    netTotal: number;

    @BelongsTo(() => Film)
    film: Film;

    @BelongsTo(() => User)
    user: User;
}