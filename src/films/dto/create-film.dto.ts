export class CreateFilmDto {
    status: string;
    name: string;
    rate: number;
    filmTypeId: number;
}